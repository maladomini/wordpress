<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
ini_set('display_errors', 'off');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'j~z,K#xEjRXd3v$!UPl?>J`UO(&Y!Bk-dgd4Q}xJtSQa^p!YyK2|>GX0JZGi.33?' );
define( 'SECURE_AUTH_KEY',  '}<UgUUkG@`WG6gxyMms+Wfkrwu2@vFQc&#pQ+`hg4/IupFF( =~&w^%W5M>^=ut|' );
define( 'LOGGED_IN_KEY',    '19.R,V~:[b4oPm#rRVQMJy|o+qg1&Q_DNWGHZ[NxZR)Y|)YSu1j}w^,6Dz4Z(WsE' );
define( 'NONCE_KEY',        '_u+fS|=Vvn$=%IMr3ZDZnK~25rq8c?ABide :a1VlMX&(U? WasJ~7)LjwUp^,gC' );
define( 'AUTH_SALT',        'apJo36nw]D0;uW>*6I*W=Z_Ww-0xeRTt<JWAPhp+X#3HUX/!)]jXc>s@bLrE`[E}' );
define( 'SECURE_AUTH_SALT', '2R{;EBA`dl787*1s^3:EI&y-5>TyK:w5iD+09$EWr((Dzi#_kpCTq07{n{SfRLgs' );
define( 'LOGGED_IN_SALT',   'OybFlXq+7k}yz?IT$x~w/2UW~;C8AL:!Mo#btrmCj1{<zo)}pD)e5lBy-WAmfDWR' );
define( 'NONCE_SALT',       'k[99^nweG|bhz67$X+#$MA1JDPDI=`&UTCAt*c9dsKCEM:u>!5Z`)=!2#l`7?YG[' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
